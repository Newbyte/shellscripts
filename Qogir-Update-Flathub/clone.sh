#!/usr/bin/env bash

distributions=( '-win' '-ubuntu' '-manjaro' '' )
variants=( '-light' '-dark' '' )

for distribution in "${distributions[@]}"
do
		for variant in "${variants[@]}"
		do
				name=org.gtk.Gtk3theme.Qogir$distribution$variant
				git clone git@github.com:Newbytee/$name
				cd $name
				git_branch_qogir='update-2021-02-0'
				git_branch_runtime='runtime-20.08'
				git remote add upstream git@github.com:flathub/$name.git
				git fetch upstream
				git checkout -b $git_branch_qogir upstream/master
				filename=$name.json
				patch $filename < ../patch2.patch
				git commit $filename -m 'Update to version 2021-02-09'
				git push -u origin $git_branch_qogir
				git checkout -b $git_branch_runtime upstream/master
				patch $filename < ../patch1.patch
				git commit $filename -m 'Build against Freedesktop Runtime 20.08'
				git push -u origin $git_branch_runtime
				cd ..
		done
done
